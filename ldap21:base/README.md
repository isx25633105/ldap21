# LDAP @isx25633105 ASIX
# Curs 2021-2022

* **isx25633105/ldap21:base:**
* Ordre per executar el container:
* Sense xarxa propia:

```

docker build -t isx25633105/ldap21:base .

docker run --rm --name lda.edt.org -h lda.edt.org -d isx25633105/ldap21:base

docker ps

ldapsearch -x -LLL -h 172.17.0.2 -b 'dc=edt,dc=org'

```

* Amb xarxa propia:

```

docker network create 2hisx
docker build -t isx25633105/ldap21:base .

docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -d isx25633105/ldap21:base

docker ps

ldapsearch -x -LLL -h ldap.edt.org -b 'dc=edt,dc=org'

```

