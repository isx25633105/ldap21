# LDAP @isx25633105 ASIX
# Curs 2021-2022

* **isx25633105/ldap21:arm** Servidor LDAP amb la base de dades edt.org
 S'ha fet el següent:
 * Adaptar la versió group per sistemes arm.
```
docker network create 2hisx
docker build -t isx25633105/ldap21:arm .
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -d isx25633105/ldap21:arm 

# No esta acabada aquesta part:
docker run --rm --name phpldapadmin.edt.org -h phpldapadmin.edt.org --net 2hisx -p 80:80 -d edtasixm06/phpldapadmin:20
```
 

