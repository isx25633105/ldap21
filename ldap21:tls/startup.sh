#! /bin/bash
#mkdir /etc/ldap/sasl2/
cp /opt/docker/cacert.pem /etc/ldap/sasl2/.
cp /opt/docker/servercert.ldap.pem /etc/ldap/sasl2/.
cp /opt/docker/serverkey.ldap.pem  /etc/ldap/sasl2/.
# Segona part ref ldap:groups
rm -rf /etc/ldap/slapd.d/*
rm -rf /var/lib/ldap/*
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt.org.ldif
chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap/ /etc/ldap/sasl2/*
cp /opt/docker/ldap.conf /etc/ldap/ldap.conf
/usr/sbin/slapd -d0 -u openldap -h "ldap:/// ldaps:/// ldapi:///"
