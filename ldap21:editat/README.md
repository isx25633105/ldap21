# LDAP @isx25633105 ASIX
# Curs 2021-2022

* **isx25633105/ldap21:editat** Servidor LDAP amb la base de dades edt.org

```
docker network create 2hisx
docker buil -t isx25633105/ldap21:editat
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -d isx25633105/ldap21:editat
```

* **Exercicis fets:**
* generar un sol fitxer ldif anomenat edt.org.ldif 

* afegir en el fitxer dos usuaris i una ou nova inventada. 

* modificar el fitxer edt.org.ldif  modificant dn dels usuaris utilitzant en lloc del cn el uid per identificar-los. 

* configurar el password de Manager que sigui ‘secret’ però encriptat (posar-hi un comentari per indicar quin és de cara a estudiar). 

* afegr el fitxer de configuració client. 

* propagar el port amb -P  -p 

