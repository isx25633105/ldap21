# LDAP @isx25633105 ASIX
# Curs 2021-2022

* **isx25633105/ldap21:base** container de treball base de ldap. Organització: edt.org
* **isx25633105/ldap21:editat** container de treball editat de ldap. Organització: edt.org
* **isx25633105/ldap21:schema**  container de treball schema de ldap. Organització: edt.org
* **isx25633105/ldap21:practica** container de treball practica de ldap. Organització: edt.org
* **isx25633105/ldap21:group** container de treball group de ldap. Organització: edt.org
* **isx25633105/ldap21:arm** container de treball arm de ldap, especial per a ser utilitzat en raspbian. Organització: edt.org
* **isx25633105/ldap21:acl** container de treball acl de ldap. Organització: edt.org
